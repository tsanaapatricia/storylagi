from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .views import story7
from .apps import Story7Config

# Create your tests here.
class UrlsTest(TestCase):
    def test_story7_use_right_function(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7)

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_GET_story7(self):
        response = self.client.get('/story7/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7.html')        

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')