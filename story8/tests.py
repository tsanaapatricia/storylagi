from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .views import story8
from .apps import Story8Config

# Create your tests here.
class UrlsTest(TestCase):
    def test_story8_use_right_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_GET_story8(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8.html')  

    def test_fungsi_data(self):
        response = self.client.get('/story8/data?q=elsa')
        self.assertEqual(response.status_code, 200)
   

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
