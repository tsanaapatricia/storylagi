$( window ).on('load', function(){
    var q = "aku"
    let daftarBuku = $('tbody');
    $.ajax({
        url: 'data?q=' + q,
        success: function(data){
            var array_items = data.items;
            console.log(array_items);
            $(daftarBuku).empty();
            for (i = 0; i < array_items.length; i++){
                var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                var judul = array_items[i].volumeInfo.title;
                var penulis = array_items[i].volumeInfo.authors;

                if(gambar ==  false){
                    var img = $("<td>").text("-");
                }

                else{
                    var img = $("<td>").append("<img src=" + gambar + ">");
                }

                var title = $("<td>").append(judul);

                if(penulis == false){
                    var aut = $("<td>").text("-");
                }

                else{
                    var aut = $("<td>").append(penulis);
                }

                var content_row = $("<tr>").append(img, title, aut);

                $(daftarBuku).append(content_row);
            }
        }
    }) 
})
$(document).ready(() => {
    $("#keyword").keyup(function(){
        var ketik = $("#keyword").val();
        console.log(ketik);
        // AJAX
        let daftarBuku = $('tbody');
        if (ketik.length){
            $.ajax({
            url: 'data?q=' + ketik,
            success: function(data){
                var array_items = data.items;
                console.log(array_items);
                $(daftarBuku).empty();
                for (i = 0; i < array_items.length; i++){
                    var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var judul = array_items[i].volumeInfo.title;
                    var penulis = array_items[i].volumeInfo.authors;

                    if(gambar ==  false){
                        var img = $("<td>").text("-");
                    }

                    else{
                        var img = $("<td>").append("<img src=" + gambar + ">");
                    }

                    var title = $("<td>").append(judul);

                    if(penulis == false){
                        var aut = $("<td>").text("-");
                    }

                    else{
                        var aut = $("<td>").append(penulis);
                    }

                    var content_row = $("<tr>").append(img, title, aut);

                    $(daftarBuku).append(content_row);
                }
            }
        }); 
        }
        $(daftarBuku).empty();
    });
})
