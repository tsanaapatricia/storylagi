$( function() {
    $( "#accordionActivity" ).accordion({
      active: false,
      collapsible: true
    });
    $( "#accordionPanit" ).accordion({
      active: false,
      collapsible: true
    }); 
    $( "#accordionPrestasi" ).accordion({
      active: false,
      collapsible: true
    });
    $( "#accordionHobi" ).accordion({
      active: false,
      collapsible: true
    });
    $('.button-up').click(function () {
      var thisAccordion = $(this).parent().parent().parent();
      thisAccordion.insertBefore(thisAccordion.prev());
      event.stopPropagation(); 
      event.preventDefault(); 
    })
    $('.button-down').click(function () {
      var thisAccordion = $(this).parent().parent().parent();
      thisAccordion.insertAfter(thisAccordion.next());
      event.stopPropagation(); 
      event.preventDefault(); 
    })   
  });