from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .views import login_page, register_page, logout_page
from .apps import Story9Config
from django.contrib.auth.models import User
from .models import Profile
from .forms import LoginForm, RegisterForm

# Create your tests here.
class UrlsTest(TestCase):
    def test_urls(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,200)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_page = reverse("story9:login")
        self.register_page = reverse("story9:register")
        self.logout_page = reverse("story9:logout")
        self.user_new = User.objects.create_user("elsa","elsa@gmail.com", password='elsafrozen')
        self.user_new.save()
        self.profile = Profile.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_page, {
            'username': 'elsa', 
            'password':'elsafrozen'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_GET_register(self):
        response = self.client.get(self.register_page, {
            'username': 'elsa', 
            'email': 'elsa@gmail.com', 
            'password_first':'elsafrozen', 
            'password_again':'elsafrozen'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    def test_register_login_post(self):
        response = self.client.post(self.register_page, data = {
            "username" : "elsa",
            "email" : "elsa@gmail.com",
            "password_first" : "elsafrozen",
            "password_again" : "elsafrozen",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "elsa",
            "email" : "elsa@gmail.com",
            "password_first" : "elsafrozen",
            "password_again" : "elsa",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "queenelsa",
            "email" : "queenelsa@gmail.com",
            "password_first" : "elsafrozen",
            "password_again" : "elsafrozen",
        })

        response = self.client.post(self.login_page,data ={
            "username" : "elsa",
            "password" : "elsafrozen"
        })
        self.assertEquals(response.status_code,302)

    def test_not_register_yet(self):
        response = self.client.post(self.login_page,data ={
            "username" : "elsaaa",
            "password" : "elsafrozen"
        })
        self.assertEquals(response.status_code,200)


  

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            "username": "elsa",
            "password": "elsafrozen",
        })

        form_regist = RegisterForm(data={
            "username": "elsa",
            "email": "elsa@gmail.com",
            "password_first" : "elsafrozen",
            "password_again" : "elsafrozen",
        })
        self.assertTrue(form_regist.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_register = RegisterForm(data={})
        self.assertFalse(form_register.is_valid())

    def test_form_regist_is_exist(self):
        form_regist = RegisterForm(data={
            "username": "elsa",
            "email": "elsa@gmail.com",
            "password_first" : "elsafrozen",
            "password_again" : "elsafrozen",
        })
        form_regist = RegisterForm(data={
            "username": "elsa",
            "email": "elsa@gmail.com",
            "password_first" : "elsafrozen",
            "password_again" : "elsafrozen",
        })
        self.assertTrue(form_regist.is_valid())

class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("elsa", password="elsafrozen")
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "elsa",
            password = "elsafrozen"
        )

    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(),1)

    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user,self.new_user)

    def test_to_string(self):
        self.assertIn("elsa",str(self.new_user.profile))
